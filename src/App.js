import React from 'react';
import SegmentedControlWrapper from './containers/SegmentedControlWrapper';
import './App.css';
import './components/SegmentedControl/SegmentedControl.css';

function App() {
  return (
    <div className="App">
      <SegmentedControlWrapper />
    </div>
  );
}

export default App;
