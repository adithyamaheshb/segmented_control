/**********************************************************************************/
// SegmentedControlWrapper is a presentational component/ dumb component which is used
// to render the radio button groups depending on the user's requirement
/**********************************************************************************/
/**********************************************************************************/

import React from 'react';
import SegmentedControl from '../components/SegmentedControl/SegmentedControl';

const SegmentedControlWrapper = () => {
    return (
        <div className="segmentedControlWrapper">
            <SegmentedControl label="Paid for by" options={["Client", "Valassis Digital"]}/>
            <SegmentedControl label="Gender" options={["All", "Male", "Female"]}/>
        </div>
    )
}

export default SegmentedControlWrapper;