/**********************************************************************************/
// props
//    @label: string
//    @options: Array of selectable values
// SegmentedControl is a reusable React Component which takes 'label' and 'options' as 
// props and creates dynamic list of radio buttons to choose from the options provided  
/**********************************************************************************/
/**********************************************************************************/


import React, { Component} from 'react';

export default class SegmentedControl extends Component {

  // Added some default Props to check the default behaviour of the dom elements via tests
  static defaultProps = {
    label: 'Paid for by',
    options: ['Client', 'Valassis Digital']
  }
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: '', // holds the selected option
    }
  }

  //event handler method to select a particlar option
  // @event: ChangeEvent
  handleChange = (event) => {
    this.setState({ selectedValue: event.target.value })
  }

  render() {
    const { selectedValue } = this.state;
    const { label, options } = this.props;
    return(
      <div className="segmentedControl">
        <span>{label}</span>
        <div className="segmentedControl__buttonGroup">
          {/* Iterating through the options provided and rendering the radio buttons irrespective
          of the number of options */}
          {options.map((option, o) => {
            return (
              <div key={o}>
                <input type="radio"
                  data-testid={`${option}`}
                  // aria-label="radioButton"
                  id={`${option}`}
                  value={`${option}`}
                  checked={selectedValue === `${option}`}
                  onChange={this.handleChange}
                   />
                <label htmlFor={`${option}`}>{option}</label>
             </div>
            )
          })}
        </div>
      </div>
    )
  }
}