import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import SegmentedControl from './SegmentedControl';

//function to unmount elements from the DOM after every test
afterEach(cleanup);

test('<SegmentedControl />', () => {
    //component rendering
    const { debug, getByTestId } = render(<SegmentedControl/>);

    //outputs the dom node you are testing
    debug();

    const radioButton = getByTestId('Client');

    //check if one of the buttons are radio button
    expect(radioButton.type).toBe('radio');
    // expect(getAllByLabelText('radioButton').type).toBe('radio');

    //check that initially no button was selected
    expect(radioButton.checked).toBe(false);

    //clicking the button
    fireEvent.click(radioButton);

    //checking whether the button has been selected after clicking on it.
    expect(radioButton.checked).toBe(true);
})