# Segmented Control
A Reusable React component which is used to render selection groups
## How to run:
1. Either clone the repository or download the repository from BitBucket.
2. Open the command terminal, copy and paste the following command: `git clone  https://adithyamaheshb@bitbucket.org/adithyamaheshb/segmented_control.git`
3. Now, navigate to the repository and type `npm install` for installing dependencies.
4. As, I used create-react-app for this application just type `npm start` after installing the dependencies.
5. Now, the application automatically opens in the browser.
6. You can see the live demo [here](http://segmented_control.surge.sh)

# Overview of the project
Basically the entire functionality is in the `src` folder. So, `containers` have the page layouts which is `SegmentedControlWrapper` at the moment and `components`
have the reusable components which can be used any number of times in the given application where they are needed and this serves the first and foremost
purpose of React. 

In the `compoments`, I have a folder called `SegmentedControl` which contains a `js`, `test` and `css` files. I would like to keep files related to particular component in
one place which makes it easy to track for future references. This approach helps when application grows in size and also improves code readability and integrity instead of dumping everything in a single file.

I have used the functional and class components in React based on the requirement.

I have included the basic suite of tests and I am using `jest` and `@testing-library/react` for performing tests on `SegmentedControl` component.

I have also hosted the application and it's live on [http://segmented_control.surge.sh](http://segmented_control.surge.sh)